// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import '@testing-library/cypress/add-commands';

Cypress.Commands.add('assertRoute', (url = null) => {
  cy.url().should('eq', `${Cypress.config().baseUrl}/${url || ''}`);
});

Cypress.Commands.add('fillForm', () => {
  cy.findByRole('textbox', { name: 'Número do cartão' }).type('5554 6786 9876 7654');
  cy.findByRole('textbox', { name: 'Nome (igual ao cartão)' }).type('PEDRO SOUZA');
  cy.findByRole('textbox', { name: 'Validade' }).type('10/27');
  cy.findByRole('textbox', { name: 'CVV' }).type('123');
  cy.findByRole('button', { name: /continuar/i }).click();
});
