describe('Checkout', () => {
  const orderId = 1;

  beforeEach(() => {
    cy.server();

    cy.route('GET', `/products/${orderId}`).as('requestProduct');

    cy.visit(`/checkout/${orderId}`);
  });

  describe('Form', () => {
    it('should show loading dialog', () => {
      cy.findByRole('dialog').should('be.visible');
    });

    it('should show form card, click on next and show errors when form is empty', () => {
      cy.findAllByRole('textbox').should('be.visible');
      cy.findByRole('button', { name: /continuar/i }).click();
      cy.findAllByText('Campo obrigatório').should('be.visible');
    });

    it('should show card number invalid message and then type valid value', () => {
      cy.findByRole('textbox', { name: 'Número do cartão' }).type('5554 678');
      cy.findByText('Cartão inválido').should('be.visible');
      cy.findByRole('textbox', { name: 'Número do cartão' }).clear().type('5554 6786 9876 7654');
      cy.findByText('Cartão inválido').should('not.exist');
    });

    it('should show date invalid message and then type valid value', () => {
      cy.findByRole('textbox', { name: 'Validade' }).type('10');
      cy.findByText('Data inválida').should('be.visible');
      cy.findByRole('textbox', { name: 'Validade' }).clear().type('22/22');
      cy.findByText('Data inválida').should('be.visible');
      cy.findByRole('textbox', { name: 'Validade' }).clear().type('15/22');
      cy.findByText('Data inválida').should('be.visible');
      cy.findByRole('textbox', { name: 'Validade' }).clear().type('12/25');
      cy.findByText('Data inválida').should('not.exist');
    });

    it('should show cvv invalid message and then type valid value', () => {
      cy.findByRole('textbox', { name: 'CVV' }).type('10');
      cy.findByText('CVV inválido').should('be.visible');
      cy.findByRole('textbox', { name: 'CVV' }).clear().type('123');
      cy.findByText('CVV inválido').should('not.exist');
    });

    it('should fill number and name, click on next and get erros', () => {
      cy.findByRole('textbox', { name: 'Número do cartão' }).type('5554 6786 9876 7654');
      cy.findByRole('textbox', { name: 'Nome (igual ao cartão)' }).type('PEDRO SOUZA');
      cy.findByRole('button', { name: /continuar/i }).click();
      cy.findAllByText('Campo obrigatório').should('be.visible');
    });

    it('should fill whole form and go to confirmation step', () => {
      cy.fillForm();
      cy.findByRole('button', { name: /pagar/i }).should('be.visible');
    });

    describe('Confirmation', () => {
      it('it should fill whole form and check confirmation product infos', () => {
        cy.fillForm();
        cy.findByText('Cerveja Antuérpia Ipa Tabla 473Ml').should('be.visible');
        cy.findAllByTestId('beer-info', (el) => {
          el.should('have.text', 'Tipo: India Pale Ale (IPA)');
          el.should('have.text', 'ABV: 6.5%');
          el.should('have.text', 'IBU: 60');
        }).should('be.visible');
      });

      it('it should fill whole form and check confirmation card infos', () => {
        cy.fillForm();
        cy.findByTestId('card-info').findAllByRole('listitem').should('be.visible');
      });

      it('it should fill whole form and pay', () => {
        cy.fillForm();
        cy.findByRole('button', { name: /pagar/i }).click();
        cy.findByText('Pagamento Aprovado').should('be.visible');
        cy.window().its('localStorage.orders').should('be.a', 'string');
        cy.assertRoute('orders');
      });
    });
  });
});
