describe('Home', () => {
  beforeEach(() => {
    cy.server();

    cy.route('GET', '/products').as('requestProducts');

    cy.visit('/');
  });

  it('should show loading dialog', () => {
    cy.findByRole('dialog').should('be.visible');
  });

  it('should show products', () => {
    cy.findAllByTestId('product-item')
      .should('be.visible')
      .and('contain', 'Cerveja Antuérpia Ipa Tabla 473Ml')
      .and('contain', 'Cerveja Bodebrown Cacau IPA 473ml')
      .and('contain', 'Cerveja Bodebrown Trooper Brasil IPA 473ml')
      .and('contain', 'Cerveja Farra Bier All In IPA 500ml');
  });

  it('should click on first product to buy', () => {
    cy.findAllByTestId('product-item')
      .should('be.visible')
      .first()
      .findByRole('button', { name: /comprar/i })
      .click()
      .url()
      .assertRoute('checkout/1');
  });

  it('should click on first product to buy and back to product list', () => {
    cy.findAllByTestId('product-item')
      .should('be.visible')
      .first()
      .findByRole('button', { name: /comprar/i })
      .click()
      .url()
      .assertRoute('checkout/1');
    cy.findByRole('link', { name: 'Produtos' }).click();
    cy.findAllByTestId('product-item').should('be.visible');
  });
});
