describe('Home', () => {
  beforeEach(() => {
    cy.server();

    cy.route('GET', '/orders').as('requestOrders');

    cy.visit('/orders');
  });

  it('should show loading dialog', () => {
    cy.findByRole('dialog').should('be.visible');
  });

  it('should show orders table', () => {
    cy.findByRole('table')
      .should('be.visible')
      .findAllByTestId('order-item')
      .should('be.visible')
      .should('have.length', 2);
  });
});
