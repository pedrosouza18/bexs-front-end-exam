<h1 align="center">Welcome to Bexs Engineering React 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
</p>

### 🏠 [Homepage](https://bexs-front-end-exam.herokuapp.com/)

### ✨ [Demo](https://bexs-front-end-exam.herokuapp.com/)

## Requirements

- [Node.js][node] 12+
- [npm][npm] (normally comes with Node.js)

[node]: https://nodejs.org/
[npm]: https://www.npmjs.com/

## Install

Antes de começar é sempre bom rodar aquele `npm i` ou `npm install` por default.

```sh
npm i
```

## Usage

```sh
npm start
```

## Build

```sh
npm run build
```

## Run tests

**OBS: Todos os testes são rodados pelo CI na etapa de deploy.**

Para rodar testes unitário é necessário rodar apenas o código abaixo:

```sh
npm t or npm test
```

Para rodar testes e2e, rode o seguinte código:

```sh
npm run test:e2
```

## Run deploy

O deploy é feito automáticamente pelo CI a cada commit nas branch principal **master**.

## Tools

- [React](https://pt-br.reactjs.org/)
- [Tailwindcss](https://tailwindcss.com/)
- [React-router](https://reactrouter.com/)
- [Mock Service Worker](https://mswjs.io/)
- [CicleCI](https://circleci.com/)
- [Cypress](https://www.cypress.io/)
- [Jest](https://jestjs.io/pt-BR/)
- [Testing Library](https://testing-library.com/)
- Heroku

## Sobre as requests REST
Foram criados 4 endpoints REST usando Mock Service Worker para simular um backend, para saber mais sobre essa lib, acesse [Mock Service Worker](https://mswjs.io/).
 
ENDPOINTS:

- /products -> Tela iniciar de listagem de produtos
- /products/:id -> Usado na tela de checkout para buscar os dados do produto escolhido
- /checkout/pay -> Utilizado para efetuar o pagamento (Pagamento fake)
- /orders -> Para buscar os pedidos pagos

## Note
Se tiver algum problema para iniciar a aplicação por causa de algum erro no `npm start`, inclua o seguinte arquivo na raiz do projeto:

**.env**
```env
SKIP_PREFLIGHT_CHECK=true
```

## Author

👤 **Pedro Souza**

- Github: [@pedrosouza18](https://github.com/pedrosouza18)
- LinkedIn: [@pedro-souza-317a22a8](https://linkedin.com/in/pedro-souza-317a22a8)

## Show your support

Give a ⭐️ if this project helped you!
