import STATUS from '../constants/constants';

export const fetching = () => ({ type: STATUS.PENDING });
export const success = (response) => ({ type: STATUS.RESOLVED, response });
export const error = (err) => ({ type: STATUS.REJECTED, error: err });
