import STATUS from '../constants/constants';

export const initialState = {
  status: null,
  response: null,
};

function reducer(state = initialState, { type, response, error } = {}) {
  switch (type) {
    case STATUS.PENDING:
      return { ...state, status: STATUS.PENDING };
    case STATUS.RESOLVED:
      return { ...state, status: STATUS.RESOLVED, response };
    case STATUS.REJECTED:
      return { ...state, status: STATUS.REJECTED, error };
    default:
      return state;
  }
}

export default reducer;
