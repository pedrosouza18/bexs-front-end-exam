import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import Navigation from './components/Navigation/Navigation';
import Home from './containers/Home/Home';
import Orders from './containers/Orders/Orders';
import Checkout from './containers/Checkout/Checkout';

function App() {
  return (
    <div className="App">
      <Navigation />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/checkout/:id">
          <Checkout />
        </Route>
        <Route path="/orders">
          <Orders />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
