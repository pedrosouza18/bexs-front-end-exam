import * as React from 'react';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import Dialog from './Dialog';

function dialogResponse({ open, message }) {
  return (
    <Dialog aria-labelledby="response-dialog" open={open}>
      <div className="p-10 text-center">
        <div>
          <DoneAllIcon className="text-green-600" style={{ fontSize: '5rem' }} />
        </div>
        <h3 className="text-xl pt-6 font-bold">{message}</h3>
      </div>
    </Dialog>
  );
}

export default dialogResponse;
