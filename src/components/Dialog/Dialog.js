import * as React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

function dialogComponent({ open, description = null, children }) {
  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open}>
      {children}
      {typeof description === 'string' ? (
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{description}</DialogContentText>
        </DialogContent>
      ) : (
        ''
      )}
    </Dialog>
  );
}

export default dialogComponent;
