import * as React from 'react';
import Button from '@material-ui/core/Button';
import { ErrorBoundary } from 'react-error-boundary';
import Dialog from '../Dialog/Dialog';

// eslint-disable-next-line react/prop-types
function ErrorFallback({ error, resetErrorBoundary }) {
  return (
    <Dialog role="alert" open className="p-8">
      <h3 className="p-6 text-xl font-bold">{error}</h3>
      <div className="flex justify-center	pb-4">
        <Button variant="contained" color="secondary" onClick={resetErrorBoundary}>
          Tentar novamente
        </Button>
      </div>
    </Dialog>
  );
}

function ErrorComponent(props) {
  return <ErrorBoundary FallbackComponent={ErrorFallback} {...props} />;
}

export default ErrorComponent;
