import * as React from 'react';

function badge({ children, filled = false }) {
  return (
    <p
      className={`flex items-center justify-center text-white w-4 border border-red-500 h-4 p-3.5 rounded-full text-sm ${
        filled ? 'bg-red-500 text-white' : 'text-red-500'
      } font-bold`}>
      <span className="font-bold">{children}</span>
    </p>
  );
}

export default badge;
