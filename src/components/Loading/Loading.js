import * as React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '../Dialog/Dialog';

function loading({ open, text }) {
  return (
    <Dialog open={open} description={text}>
      <div className="py-5 text-center">
        <CircularProgress color="secondary" />
      </div>
    </Dialog>
  );
}

export default loading;
