import * as React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import Product from './Product/Product';

function Products({ products }) {
  const history = useHistory();

  const chooseProduct = (id) => {
    history.push(`/checkout/${id}`);
  };

  return (
    <section className="grid grid-flow-row sm:grid-cols-2">
      {products.map((prd) => (
        <Product key={prd.id} product={prd} selectProduct={chooseProduct} />
      ))}
    </section>
  );
}

Products.defaultProps = {
  products: [],
};

Products.propTypes = {
  products: PropTypes.instanceOf(Array),
};

export default Products;
