import * as React from 'react';
import Card from '@material-ui/core/Card';
import { CardActions, CardContent, CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Utils from '../../../utils/utils';

function Product({ product, selectProduct }) {
  const { image, title, type, abv, ibu, id, price } = product;

  const refPlaceholder = React.useRef();

  const removePlaceholder = () => {
    refPlaceholder.current.remove();
  };

  return (
    <>
      <Card className="m-5 flex flex-col justify-between" data-testid="product-item">
        <CardContent>
          <div className="relative w-full h-52">
            <div className="absolute inset-0 flex justify-center items-center" ref={refPlaceholder}>
              <CircularProgress color="secondary" />
            </div>
            <LazyLoad>
              <img
                className="absolute w-full h-full left-0 object-contain"
                src={image}
                alt={title}
                onLoad={removePlaceholder}
                onError={removePlaceholder}
              />
            </LazyLoad>
          </div>
          <section>
            <h3 className="text-center">
              <strong>{title}</strong>
            </h3>
            <div className="pt-6">
              <ul className="flex justify-between flex-wrap text-sm">
                <li>
                  <b>Tipo: </b> {type}
                </li>
                <li>
                  <b>ABV: </b> {abv}%
                </li>
                <li>
                  <b>IBU: </b> {ibu}
                </li>
              </ul>
            </div>
            <p className="pt-4 text-center">
              <strong>{Utils.formatAmount(price)}</strong>
            </p>
          </section>
        </CardContent>
        <CardActions>
          <Button
            variant="contained"
            color="secondary"
            size="medium"
            className="w-full"
            onClick={() => selectProduct(id)}>
            Comprar
          </Button>
        </CardActions>
      </Card>
    </>
  );
}

Product.defaultProps = {
  product: {},
};

Product.propTypes = {
  product: PropTypes.instanceOf(Object),
  selectProduct: PropTypes.func.isRequired,
};

export default Product;
