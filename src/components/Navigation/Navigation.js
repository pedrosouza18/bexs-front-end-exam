import * as React from 'react';
import { NavLink } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import Logo from '../../assets/logo.png';

function Navigation() {
  const [mobileMenuOpen, setMobileMenuOpen] = React.useState(false);

  return (
    <header className="flex flex-wrap sticky top-0 flex-row justify-between shadow z-10 items-center md:space-x-4 bg-white py-6 px-6 lg:px-20 relative">
      <div className="block">
        <span className="sr-only">themes.dev</span>
        <img className="h-6 md:h-8" src={Logo} alt="Logo Loja" title="Logo Loja" />
      </div>
      <button
        onClick={() => setMobileMenuOpen(!mobileMenuOpen)}
        type="button"
        className="inline-block md:hidden w-8 h-8 p-1">
        {mobileMenuOpen ? <CloseIcon /> : <MenuIcon />}
      </button>
      <nav
        className={`absolute md:relative top-16 left-0 md:top-0 z-20 md:flex ${
          mobileMenuOpen ? 'flex' : 'hidden'
        } flex-col md:flex-row md:space-x-6 shadow-md font-semibold w-full md:w-auto bg-white rounded-lg md:rounded-none md:shadow-none md:bg-transparent p-5 pt-0 md:p-0`}>
        <NavLink
          to="/"
          exact
          activeClassName="text-red-500"
          className="block py-1 text-gray-600 hover:underline">
          Produtos
        </NavLink>
        <NavLink
          to="/orders"
          activeClassName="text-red-500"
          className="block py-1 text-gray-600 hover:underline">
          Pedidos
        </NavLink>
      </nav>
    </header>
  );
}

export default Navigation;
