import * as React from 'react';
import { render as rtlRender } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

function render(
  ui,
  { route = '/', history = createMemoryHistory({ initialEntries: [route] }), ...renderOptions } = {}
) {
  /* eslint react/prop-types: 0 */
  function Wrapper({ children }) {
    return <Router history={history}>{children}</Router>;
  }
  return {
    ...rtlRender(ui, {
      wrapper: Wrapper,
      ...renderOptions,
    }),
    history,
  };
}

export default render;
