import Payment from 'payment';

export default {
  clearNumber(value = '') {
    return value.replace(/\D+/g, '');
  },
  formatAmount(amount) {
    const parseAmount = Number(amount);
    return parseAmount.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
  },
  formatCreditCardNumber(value) {
    if (!value) {
      return value;
    }

    const issuer = Payment.fns.cardType(value);
    const clearValue = this.clearNumber(value);
    let nextValue;

    switch (issuer) {
      case 'amex':
        nextValue = `${clearValue.slice(0, 4)} ${clearValue.slice(4, 10)} ${clearValue.slice(
          10,
          15
        )}`;
        break;
      case 'dinersclub':
        nextValue = `${clearValue.slice(0, 4)} ${clearValue.slice(4, 10)} ${clearValue.slice(
          10,
          14
        )}`;
        break;
      default:
        nextValue = `${clearValue.slice(0, 4)} ${clearValue.slice(4, 8)} ${clearValue.slice(
          8,
          12
        )} ${clearValue.slice(12, 19)}`;
        break;
    }

    return nextValue.trim();
  },
  formatExpirationDate(value) {
    const clearValue = this.clearNumber(value);

    if (clearValue.length >= 3) {
      return `${clearValue.slice(0, 2)}/${clearValue.slice(2, 4)}`;
    }

    return clearValue;
  },
  mergeObjectsWithoutNullValues(obj1, obj2) {
    const res = {};
    Object.entries({ ...obj1, ...obj2 }).forEach(([key, value]) => {
      res[key] = typeof value !== 'undefined' ? value : obj2[key] || obj1[key];
    });
    return res;
  },
  getLocalStorageItem(key, initialValue) {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (e) {
      return initialValue;
    }
  },
  setLocalStorageItem(key, value) {
    try {
      const valueToStore = value instanceof Function ? value(value) : value;
      window.localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {
      throw new Error('unheandle set value');
    }
  },
};
