import * as React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@material-ui/core';
import Loading from '../../components/Loading/Loading';
import ProductService from '../../services/ProductService';
import { success, error, fetching } from '../../hooks/actionCreators';
import reducer, { initialState } from '../../hooks/reducer';
import Utils from '../../utils/utils';
import STATUS from '../../constants/constants';

function maskCardNumber(card) {
  const num = card.split(' ');
  return `${num[0]} ***** ${num[num.length - 1]}`;
}

function Orders() {
  const [{ response, status }, dispatch] = React.useReducer(reducer, initialState);
  const mounted = React.useRef(true);

  React.useEffect(() => {
    const { ready, abort } = ProductService.getOrders();

    dispatch(fetching());
    ready
      .then(async (result) => {
        if (mounted.current) {
          const data = await result.json();
          dispatch(success(data));
        }
      })
      .catch(() => {
        if (mounted.current) {
          const err = 'Não foi possível buscar o produto!';
          dispatch(error(err));
        }
      });

    return () => {
      mounted.current = false;
      abort();
    };
  }, []);

  return (
    <section className="max-w-screen-md mx-auto md:mt-16">
      {status === STATUS.PENDING ? <Loading text="Carregando pedidos ..." open /> : null}
      {status === STATUS.RESOLVED ? (
        <TableContainer className="bg-white shadow">
          <Table aria-label="custom pagination table">
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Produto</TableCell>
                <TableCell>Nome</TableCell>
                <TableCell>Cartão</TableCell>
                <TableCell>Valor</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {response.length > 0 ? (
                response.map((order) => (
                  <TableRow key={order.orderId} data-testid="order-item">
                    <TableCell component="th" scope="row">
                      {order.orderId}
                    </TableCell>
                    <TableCell>
                      <b>{order.productInfo.title}</b>
                    </TableCell>
                    <TableCell>
                      <b>{order.cardData.name}</b>
                    </TableCell>
                    <TableCell>{maskCardNumber(order.cardData.number)}</TableCell>
                    <TableCell>
                      <b>{Utils.formatAmount(order.productInfo.price)}</b>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow style={{ height: 53 }}>
                  <TableCell colSpan={5} align="center">
                    Você ainda não possui pedidos.
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      ) : null}
    </section>
  );
}

export default Orders;
