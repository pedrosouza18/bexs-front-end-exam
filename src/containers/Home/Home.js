import * as React from 'react';
import Products from '../../components/Products/Products';
import ProductService from '../../services/ProductService';
import { success, error, fetching } from '../../hooks/actionCreators';
import reducer, { initialState } from '../../hooks/reducer';
import ErrorComponent from '../../components/Error/Error';
import Loading from '../../components/Loading/Loading';
import STATUS from '../../constants/constants';

function getTemplate(status, response, err) {
  switch (status) {
    case STATUS.PENDING:
      return <Loading text="Carregando produtos ..." open />;
    case STATUS.RESOLVED:
      return <Products products={response} />;
    case STATUS.REJECTED:
      throw err;
    default:
      break;
  }
  return null;
}

function ProductList({ response, err, status }) {
  return getTemplate(status, response, err);
}

function Home() {
  const [{ response, error: errorMessage, status }, dispatch] = React.useReducer(
    reducer,
    initialState
  );
  const abortRef = React.useRef(null);
  const mounted = React.useRef(true);

  function fetchProducts() {
    const { ready, abort } = ProductService.getProducts();
    abortRef.current = abort;
    dispatch(fetching());
    ready
      .then(async (result) => {
        if (mounted.current) {
          const data = await result.json();
          dispatch(success(data));
        }
      })
      .catch(() => {
        if (mounted.current) {
          const err = 'Não foi possível buscar os produtos!';
          dispatch(error(err));
        }
      });
  }

  React.useEffect(() => {
    fetchProducts();

    return () => {
      mounted.current = false;
      abortRef.current();
      abortRef.current = null;
    };
  }, []);

  return (
    <section className="max-w-screen-md mx-auto md:mt-12">
      <ErrorComponent resetKeys={[status]} onReset={fetchProducts}>
        <ProductList status={status} response={response} err={errorMessage} />
      </ErrorComponent>
    </section>
  );
}

export default Home;
