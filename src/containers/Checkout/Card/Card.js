import * as React from 'react';
import { TextField, Select, FormControl, InputLabel } from '@material-ui/core';
import PropTypes from 'prop-types';
import Utils from '../../../utils/utils';

function maskInput(value, type) {
  switch (type) {
    case 'card':
      return Utils.formatCreditCardNumber(value);
    case 'date':
      return Utils.formatExpirationDate(value);
    default:
      break;
  }
  return value;
}

function Card({ register, handleInputFocus, formState }) {
  const maxLength = (evt, { mxLength, type }) => {
    const event = evt;
    const { value } = event.target;
    const isvalidType = typeof type === 'string';
    const mxValue = isvalidType
      ? Math.max(0, parseInt(value, 10)).toString().slice(0, mxLength)
      : value.slice(0, mxLength);
    const formatValue = isvalidType ? maskInput(value, type) : mxValue;
    event.target.value = !Number.isNaN(+mxValue) ? formatValue : '';
    return event.target.value;
  };

  return (
    <section className="max-w-screen-md mx-auto mt-2 lg:mt-4 px-8 lg:pl-0 lg:pr-10">
      <form>
        <div className="pb-4 pt-6">
          <TextField
            required
            id="card-number"
            aria-labelledby="card-number"
            onFocus={handleInputFocus}
            name="number"
            label="Número do cartão"
            {...register('number', {
              required: {
                value: true,
                message: 'Campo obrigatório',
              },
              pattern: {
                value: /\d+/,
                message: 'Apenas números',
              },
              minLength: {
                value: 17,
                message: 'Cartão inválido',
              },
              shouldUnregister: true,
            })}
            className="w-full"
            error={Boolean(formState?.errors?.number)}
            helperText={formState?.errors?.number?.message}
            onInput={(e) => maxLength(e, { mxLength: 16, type: 'card' })}
          />
        </div>
        <div className="pb-4">
          <TextField
            required
            onFocus={handleInputFocus}
            id="card-holder"
            name="name"
            label="Nome (igual ao cartão)"
            {...register('name', {
              required: {
                value: true,
                message: 'Campo obrigatório',
              },
              shouldUnregister: true,
            })}
            className="w-full"
            error={Boolean(formState?.errors?.name)}
            helperText={formState?.errors?.name?.message}
            onInput={(e) => {
              const event = e;
              event.target.value = e.target.value.toUpperCase();
              return event.target.value;
            }}
          />
        </div>
        <div className="flex">
          <div className="pr-2 pb-4 w-6/12	">
            <TextField
              required
              onFocus={handleInputFocus}
              id="expired"
              label="Validade"
              name="expiry"
              className="w-full"
              {...register('expiry', {
                required: {
                  value: true,
                  message: 'Campo obrigatório',
                },
                pattern: {
                  value: /^(0[1-9]|1[012])[- /.]([2-9]{1}[2-9]{1})$/,
                  message: 'Data inválida',
                },
                minLength: {
                  value: 4,
                  message: 'Data inválida',
                },
                shouldUnregister: true,
              })}
              error={Boolean(formState?.errors?.expiry)}
              helperText={formState?.errors?.expiry?.message}
              onInput={(e) => maxLength(e, { mxLength: 4, type: 'date' })}
            />
          </div>
          <div className="pl-2 pb-4 w-6/12">
            <TextField
              required
              onFocus={handleInputFocus}
              id="cvv"
              label="CVV"
              className="w-full"
              name="cvc"
              {...register('cvc', {
                required: {
                  value: true,
                  message: 'Campo obrigatório',
                },
                pattern: {
                  value: /\d+/,
                  message: 'Apenas números',
                },
                minLength: {
                  value: 3,
                  message: 'CVV inválido',
                },
                shouldUnregister: true,
              })}
              error={Boolean(formState?.errors?.cvc)}
              helperText={formState?.errors?.cvc?.message}
              onInput={(e) => maxLength(e, { mxLength: 4 })}
            />
          </div>
        </div>
        <div className="pb-6">
          <FormControl className="w-full">
            <InputLabel htmlFor="installments">Número de parcelas</InputLabel>
            <Select
              native
              id="installments"
              name="installments"
              data-testid="select-installments"
              onFocus={handleInputFocus}
              {...register('installments', { shouldUnregister: true })}>
              <option value="1">À vista</option>
              <option value="2">Parcelado 2x</option>
              <option value="3">Parcelado 3x</option>
            </Select>
          </FormControl>
        </div>
      </form>
    </section>
  );
}

Card.propTypes = {
  register: PropTypes.func.isRequired,
  handleInputFocus: PropTypes.func.isRequired,
  formState: PropTypes.instanceOf(Object).isRequired,
};

export default Card;
