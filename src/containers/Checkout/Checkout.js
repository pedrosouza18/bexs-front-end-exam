import * as React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import Button from '@material-ui/core/Button';
import Cards from 'react-credit-cards';
import { ArrowBackIos, ArrowForwardIos, Check } from '@material-ui/icons';
import 'react-credit-cards/es/styles-compiled.css';
import CardForm from './Card/Card';
import CardImage from '../../assets/card-logo.png';
import Confirmation from './Confirmation/Confirmation';
import Badge from '../../components/Badge/Badge';
import ProductService from '../../services/ProductService';
import STATUS from '../../constants/constants';
import { success, error, fetching } from '../../hooks/actionCreators';
import reducer, { initialState } from '../../hooks/reducer';
import Loading from '../../components/Loading/Loading';
import Utils from '../../utils/utils';
import DialogResponse from '../../components/Dialog/DialogResponse';
import './Checkout.css';

function getSteps() {
  return ['Carrinho', 'Pagamento', 'Confirmação'];
}

function getStepContent(step, formConfig, product, cardData) {
  switch (step) {
    case 1:
      return <CardForm {...formConfig} />;
    case 2:
      return <Confirmation product={product} cardData={cardData} />;
    default:
      throw new Error('Page not found');
  }
}

const initialForm = {
  number: '',
  name: '',
  expiry: '',
  cvc: '',
  installments: 1,
};

function Checkout() {
  const { id } = useParams();
  const history = useHistory();
  const [{ status, response }, dispatch] = React.useReducer(reducer, initialState);
  const [formFieldValues, setFormFieldValues] = React.useState(initialForm);
  const { register, formState, trigger, watch } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  });
  const [activeStep, setActiveStep] = React.useState(1);
  const [inputFocused, setInputFocused] = React.useState('');
  const [loadingText, setLoadingText] = React.useState('Carregando carrinho ...');
  const [paymentSuccess, setPaymentSuccess] = React.useState(false);
  const steps = getSteps();
  const watchAllFields = watch();
  const mounted = React.useRef(true);

  const handleInputFocus = ({ target }) => {
    setInputFocused(target.name);
  };

  const pay = (payload) => {
    setLoadingText('Processando pagamento ...');
    const { ready } = ProductService.pay(payload);
    dispatch(fetching());
    ready
      .then(() => {
        setPaymentSuccess(true);
        setTimeout(() => {
          history.replace('/orders');
        }, 2000);
      })
      .catch(() => {
        const err = 'Erro ao processar o pagamento!';
        throw err;
      });
  };

  const handleNext = () => {
    const { isValid, isDirty } = formState;
    if (activeStep === steps.length - 1) {
      pay({ cardData: { ...formFieldValues }, productInfo: { ...response } });
      return;
    }
    if (activeStep < steps.length - 1) {
      const fieldsValid = Object.values(watchAllFields).some((val) => typeof val !== 'undefined');
      if ((!isDirty && !fieldsValid) || !isValid) {
        trigger();
        return;
      }
      const mergedObjects = Utils.mergeObjectsWithoutNullValues(formFieldValues, watchAllFields);
      setFormFieldValues(mergedObjects);
      setInputFocused('');
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setFormFieldValues(initialForm);
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(1);
  };

  React.useEffect(() => {
    const { ready, abort } = ProductService.getProductById(id);
    dispatch(fetching());
    ready
      .then(async (result) => {
        if (mounted.current) {
          const data = await result.json();
          dispatch(success(data));
        }
      })
      .catch(() => {
        if (mounted.current) {
          const err = 'Não foi possível buscar o produto!';
          dispatch(error(err));
        }
      });

    return () => {
      mounted.current = false;
      abort();
    };
  }, [id]);

  return (
    <section className="max-w-screen-sm	lg:max-w-screen-lg mx-auto md:mt-16 md:mb-16 bg-white lg:flex">
      {status === STATUS.PENDING && !paymentSuccess ? <Loading text={loadingText} open /> : null}
      {paymentSuccess ? <DialogResponse message="Pagamento Aprovado" open /> : null}
      <div className="card-container">
        <section className="py-3.5 lg:py-0">
          <div className="text-center py-5 lg:pb-12 lg:pt-0 relative flex justify-around items-center">
            {activeStep === steps.length - 1 ? (
              <div className="absolute left-0">
                <Button
                  style={{ textTransform: 'none', color: 'white', fontSize: '0.65rem' }}
                  onClick={handleBack}>
                  <ArrowBackIos className="text-white" />
                  <span className="hidden lg:block">Alterar forma de pagamento</span>
                </Button>
              </div>
            ) : null}
            <p className="text-white lg:w-full lg:pl-7 lg:text-left">
              <span className="block lg:hidden">
                <b>Etapa {activeStep + 1}</b> de {steps.length}
              </span>
            </p>
          </div>
          <div className="flex items-center w-72 mx-auto lg:m-0 lg:w-56 lg:pb-6">
            <img src={CardImage} alt="Imagem cartão" />
            <h4 className="text-white font-bold pl-3 text-sm lg:text-base">
              Adicione um novo cartão de crédito
            </h4>
          </div>
        </section>
        <div className="card-box">
          <Cards
            cvc={watchAllFields.cvc || formFieldValues.cvc || ''}
            expiry={watchAllFields.expiry || formFieldValues.expiry || ''}
            focused={inputFocused}
            name={watchAllFields.name || formFieldValues.name || ''}
            number={watchAllFields.number || formFieldValues.number || ''}
            placeholders={{ name: 'SEU NOME' }}
            className="md:m-0"
          />
        </div>
      </div>
      <div className="w-full lg:pr-8">
        <div className="items-center justify-center pt-9 pb-5 hidden lg:flex">
          <ul className="flex justify-center items-center">
            {[...steps].map((step, index) => (
              <li className="pr-6 text-xl flex items-center" key={step}>
                <span className="pr-4 flex text-red-500 text-base items-center">
                  <Badge filled={activeStep >= index + 1}>
                    {activeStep >= index + 1 ? <Check /> : index + 1}
                  </Badge>
                  &nbsp; {step}
                </span>
                {steps.length - 1 > index ? <ArrowForwardIos className="text-red-500" /> : null}
              </li>
            ))}
          </ul>
        </div>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Button onClick={handleReset}>Pagar</Button>
            </div>
          ) : (
            <div className="pb-5">
              {getStepContent(
                activeStep,
                {
                  register,
                  handleInputFocus,
                  formState,
                },
                response,
                formFieldValues
              )}
              <div className="px-6 lg:px-0 text-right lg:py-8">
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={handleNext}
                  className={activeStep < 1 ? 'w-2/4' : 'w-full md:w-2/4'}>
                  {activeStep === steps.length - 1 ? 'Pagar' : 'Continuar'}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
  );
}

export default Checkout;
