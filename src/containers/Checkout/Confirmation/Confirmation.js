import * as React from 'react';
import PropTypes from 'prop-types';
import Utils from '../../../utils/utils';

function Confirmation({ product, cardData }) {
  return (
    <section className="max-w-screen-md mx-auto mt-2 mt-6 px-8 lg:pl-0 lg:pr-10 grid grid-flow-row sm:grid-cols-2">
      <div className="lg:px-4 lg:border-r">
        <h3 className="text-center font-bold text-lg pb-6">{product.title}</h3>
        <ul className="flex justify-between flex-wrap text-sm pb-4">
          <li data-testid="beer-info">
            <b>Tipo: </b> {product.type}
          </li>
          <li data-testid="beer-info">
            <b>ABV: </b> {product.abv}%
          </li>
          <li data-testid="beer-info">
            <b>IBU: </b> {product.ibu}
          </li>
        </ul>
        <p className="text-sm" data-testid="description">
          {product.description}
        </p>
        <p className="pt-4 text-lg" data-testid="total">
          <b>Total:</b> {Utils.formatAmount(product.price)}
        </p>
      </div>
      <div className="lg:px-4 py-8 lg:py-0" data-testid="card-info">
        <h3 className="text-center font-bold text-lg pb-6">Dados do cartão</h3>
        <ul className="pb-4">
          <li className="border-b-2 py-2">
            <b>Número: </b> {cardData.number}
          </li>
          <li className="border-b-2 py-2">
            <b>Nome: </b> {cardData.name}
          </li>
          <li className="border-b-2 py-2">
            <b>Validade: </b> {cardData.expiry}
          </li>
          <li className="py-2">
            <b>CVV: </b> {cardData.cvc}
          </li>
        </ul>
      </div>
    </section>
  );
}

Confirmation.propTypes = {
  product: PropTypes.instanceOf(Object).isRequired,
  cardData: PropTypes.instanceOf(Object).isRequired,
};

export default Confirmation;
