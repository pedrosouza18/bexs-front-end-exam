import * as React from 'react';
import { waitFor, within } from '@testing-library/react';
import render from '../utils/render';
import Home from '../containers/Home/Home';

describe('Home component', () => {
  test('it should show loading dialog when is loading products', async () => {
    const { getByRole } = render(<Home />);
    await waitFor(() => expect(getByRole('dialog')).toBeInTheDocument());
  });

  test('it should show products on list', async () => {
    const { getAllByTestId } = render(<Home />);
    await waitFor(() => {
      const products = getAllByTestId('product-item');
      expect(products.length).toBe(4);
      products.forEach((prd) => {
        expect(prd).toBeInTheDocument();
      });
    });
  });

  test('it should show the product title', async () => {
    const { getAllByTestId } = render(<Home />);
    await waitFor(() => {
      const products = getAllByTestId('product-item');
      const titles = products.map((prd) => within(prd).getByRole('heading').textContent);
      expect(titles).toMatchInlineSnapshot(`
        Array [
          "Cerveja Antuérpia Ipa Tabla 473Ml",
          "Cerveja Bodebrown Cacau IPA 473ml",
          "Cerveja Bodebrown Trooper Brasil IPA 473ml",
          "Cerveja Farra Bier All In IPA 500ml",
        ]
      `);
    });
  });
});
