import * as React from 'react';
import { waitFor, screen, within } from '@testing-library/react';
import render from '../utils/render';
import Orders from '../containers/Orders/Orders';
import { server, rest } from '../mocks/server';

describe.only('Home component', () => {
  test('it should show no orders found', async () => {
    server.use(
      rest.get('/orders', async (req, res, ctx) => {
        return res.once(ctx.status(200), ctx.json([]));
      })
    );

    render(<Orders />, {
      route: '/orders',
    });

    await waitFor(() => {
      expect(screen.getByText('Você ainda não possui pedidos.'));
    });
  });

  test('it should show orders', async () => {
    render(<Orders />, {
      route: '/orders',
    });

    await waitFor(() => {
      const products = screen.getAllByTestId('order-item');
      const orderValues = products.map((ord) =>
        within(ord)
          .getAllByRole('cell')
          .map((it) => it.textContent)
          .join(', ')
      );
      expect(products).toHaveLength(2);
      expect(orderValues).toMatchInlineSnapshot(`
        Array [
          "1, Cerveja Antuérpia Ipa Tabla 473Ml, PEDRO SOUZA, 5226 ***** 7654, R$100.00",
          "2, Cerveja Bodebrown Cacau IPA 473ml, RAFAELA SOUZA, 5226 ***** 9635, R$150.00",
        ]
      `);
    });
  });
});
