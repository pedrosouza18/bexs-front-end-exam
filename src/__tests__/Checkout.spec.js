import * as React from 'react';
import { waitFor, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useParams } from 'react-router-dom';
import render from '../utils/render';
import Checkout from '../containers/Checkout/Checkout';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest.fn(),
}));

jest.mock('react-credit-cards');

describe('Checkout component', () => {
  const card = {
    number: '5226 8543 7789 7654',
    name: 'PEDRO SOUZA',
    expiry: '12/28',
    cvc: '123',
    installments: 2,
  };

  beforeEach(() => {
    useParams.mockReturnValue({ id: '2' });
  });

  describe('Form Component', () => {
    test('it should show required errors on form when all fields are empty', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });
      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        const invalidMessages = screen.getAllByText('Campo obrigatório');
        expect(invalidMessages.length).toBeGreaterThan(0);
        invalidMessages.forEach((msg) => expect(msg).toBeInTheDocument());
        expect(screen.getByRole('button', { name: /continuar/i })).toBeInTheDocument();
        expect(screen.queryByRole('button', { name: /Pagar/i })).not.toBeInTheDocument();
      });
    });

    test('it should show card invalid error when card number length is invalid', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });
      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        const cardInput = screen.getByRole('textbox', { name: 'Número do cartão' });
        userEvent.type(cardInput, '5226');
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        const cardInvalidMessage = screen.getByText('Cartão inválido');
        expect(cardInvalidMessage).toBeInTheDocument();
        expect(screen.getByRole('button', { name: /continuar/i })).toBeInTheDocument();
        expect(screen.queryByRole('button', { name: /Pagar/i })).not.toBeInTheDocument();
      });
    });

    test('it should show date invalid error', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });
      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        const dateInput = screen.getByRole('textbox', { name: 'Validade' });
        userEvent.type(dateInput, '00/00');
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        const dateInvalidMessage = screen.getByText('Data inválida');
        expect(dateInvalidMessage).toBeInTheDocument();
        expect(screen.getByRole('button', { name: /continuar/i })).toBeInTheDocument();
        expect(screen.queryByRole('button', { name: /Pagar/i })).not.toBeInTheDocument();
      });

      await waitFor(() => {
        const dateInput = screen.getByRole('textbox', { name: 'Validade' });
        userEvent.type(dateInput, '00');
        expect(screen.getByText('Data inválida')).toBeInTheDocument();
      });
    });

    test('it should show CVV invalid error', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });
      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        const cvvInput = screen.getByRole('textbox', { name: 'CVV' });
        userEvent.type(cvvInput, '0');
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        const cvvInvalidMessage = screen.getByText('CVV inválido');
        expect(cvvInvalidMessage).toBeInTheDocument();
        expect(screen.getByRole('button', { name: /continuar/i })).toBeInTheDocument();
        expect(screen.queryByRole('button', { name: /Pagar/i })).not.toBeInTheDocument();
      });
    });

    test('it should fill whole form and go to confirmation step', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });

      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        userEvent.type(screen.getByRole('textbox', { name: 'Número do cartão' }), card.number);
        userEvent.type(screen.getByRole('textbox', { name: 'Nome (igual ao cartão)' }), card.name);
        userEvent.type(screen.getByRole('textbox', { name: 'Validade' }), card.expiry);
        userEvent.type(screen.getByRole('textbox', { name: 'CVV' }), card.cvc);
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        expect(screen.queryByRole('button', { name: /continuar/i })).toBe(null);
        expect(screen.queryByRole('button', { name: /Pagar/i })).toBeInTheDocument();
      });
    });
  });

  describe('Confirmation component', () => {
    test('it should fill whole form and check confirmation product infos', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });

      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        userEvent.type(screen.getByRole('textbox', { name: 'Número do cartão' }), card.number);
        userEvent.type(screen.getByRole('textbox', { name: 'Nome (igual ao cartão)' }), card.name);
        userEvent.type(screen.getByRole('textbox', { name: 'Validade' }), card.expiry);
        userEvent.type(screen.getByRole('textbox', { name: 'CVV' }), card.cvc);
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        expect(screen.queryByRole('button', { name: /continuar/i })).toBe(null);
        expect(screen.queryByRole('button', { name: /Pagar/i })).toBeInTheDocument();
      });

      await waitFor(() => {
        expect(screen.getByText('Cerveja Bodebrown Cacau IPA 473ml')).toBeInTheDocument();
        const beerInfos = screen.getAllByTestId('beer-info');
        const beerInfosContent = beerInfos.map((beer) => beer.textContent);
        expect(beerInfosContent).toMatchInlineSnapshot(`
          Array [
            "Tipo:  American IPA",
            "ABV:  6.1%",
            "IBU:  35",
          ]
        `);
        const description = screen.getByTestId('description');
        const total = screen.getByTestId('total');
        expect(description).toBeInTheDocument();
        expect(total).toBeInTheDocument();
        expect(description.textContent).toMatchInlineSnapshot(
          `"É uma cerveja AMERICAN IPA COM CACAU feita em colaboração com a cervejaria norte-americana Stone Brewing® da Califórnia durante a visita do CEO & FUNDADOR GREG KOCH à BODEBROWN®."`
        );
        expect(total.textContent).toMatchInlineSnapshot(`"Total: R$150.00"`);
      });
    });

    test('it should fill whole form and check confirmation card infos', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });

      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        userEvent.type(screen.getByRole('textbox', { name: 'Número do cartão' }), card.number);
        userEvent.type(screen.getByRole('textbox', { name: 'Nome (igual ao cartão)' }), card.name);
        userEvent.type(screen.getByRole('textbox', { name: 'Validade' }), card.expiry);
        userEvent.type(screen.getByRole('textbox', { name: 'CVV' }), card.cvc);
        userEvent.click(nextButton);
      });

      await waitFor(() => {
        expect(screen.queryByRole('button', { name: /continuar/i })).toBe(null);
        expect(screen.queryByRole('button', { name: /Pagar/i })).toBeInTheDocument();
      });

      await waitFor(() => {
        const cardContainer = screen.getByTestId('card-info');
        const cardInfo = within(cardContainer).getAllByRole('listitem');
        expect(cardInfo).toHaveLength(4);
      });
    });

    test('it should fill whole form go to confirmation and pay order', async () => {
      render(<Checkout />, {
        route: '/checkout/2',
      });

      await waitFor(() => {
        const nextButton = screen.getByRole('button', { name: /continuar/i });
        userEvent.type(screen.getByRole('textbox', { name: 'Número do cartão' }), card.number);
        userEvent.type(screen.getByRole('textbox', { name: 'Nome (igual ao cartão)' }), card.name);
        userEvent.type(screen.getByRole('textbox', { name: 'Validade' }), card.expiry);
        userEvent.type(screen.getByRole('textbox', { name: 'CVV' }), card.cvc);
        userEvent.click(nextButton);
      });

      userEvent.click(screen.queryByRole('button', { name: /Pagar/i }));

      await waitFor(() => {
        expect(screen.getByText('Pagamento Aprovado')).toBeInTheDocument();
      });
    });
  });
});
