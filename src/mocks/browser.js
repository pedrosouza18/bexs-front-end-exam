import { setupWorker } from 'msw';
import Handlers from './handlers';

const worker = setupWorker(...Handlers);

export default worker;
