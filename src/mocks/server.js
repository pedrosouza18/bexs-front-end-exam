import { rest } from 'msw';
import { setupServer } from 'msw/node';
import Handlers from './handlers';

const server = setupServer(...Handlers);

export { server, rest };
