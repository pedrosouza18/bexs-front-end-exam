import cacauIpa from '../assets/cacau-ipa.png';
import trooper from '../assets/trooper.png';
import antuerpia from '../assets/antuerpia.png';
import farra from '../assets/farra.png';

export const products = [
  {
    id: 1,
    title: 'Cerveja Antuérpia Ipa Tabla 473Ml',
    description:
      'Uma seleção de oito variedades de lúpulos americanos confere à Antuérpia Tabla um amargor intenso, porém equilibrado, com notas de cítrico, floral e frutado.',
    type: 'India Pale Ale (IPA)',
    abv: 6.5,
    ibu: 60,
    image: antuerpia,
    price: 100,
  },
  {
    id: 2,
    title: 'Cerveja Bodebrown Cacau IPA 473ml',
    description:
      'É uma cerveja AMERICAN IPA COM CACAU feita em colaboração com a cervejaria norte-americana Stone Brewing® da Califórnia durante a visita do CEO & FUNDADOR GREG KOCH à BODEBROWN®.',
    type: 'American IPA',
    abv: 6.1,
    ibu: 35,
    image: cacauIpa,
    price: 150,
  },
  {
    id: 3,
    title: 'Cerveja Bodebrown Trooper Brasil IPA 473ml',
    description:
      'A cerveja Trooper Brasil IPA, foi criada e desenvolvida com o vocalista do Iron Maiden, Bruce Dickinson e os irmãos Cavalcanti da Cervejaria Brodebrown.',
    type: 'India Pale Ale (IPA)',
    abv: 5.0,
    ibu: 40,
    image: trooper,
    price: 200,
  },
  {
    id: 4,
    title: 'Cerveja Farra Bier All In IPA 500ml',
    description:
      'Grande aposta, fomos de all in nessa receita, colocamos todas as fichas na mesa assim como na jogada do Poker.',
    type: 'American IPA',
    abv: 7.0,
    ibu: 70,
    image: farra,
    price: 350,
  },
];

export const orders = [
  {
    orderId: 1,
    productInfo: {
      title: products[0].title,
      price: products[0].price,
    },
    cardData: {
      number: '5226 8543 7789 7654',
      name: 'PEDRO SOUZA',
      expiry: '12/28',
      cvc: '123',
      installments: 1,
    },
  },
  {
    orderId: 2,
    productInfo: {
      title: products[1].title,
      price: products[1].price,
    },
    cardData: {
      number: '5226 8543 7789 9635',
      name: 'RAFAELA SOUZA',
      expiry: '12/30',
      cvc: '123',
      installments: 2,
    },
  },
];
