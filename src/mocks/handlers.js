import { rest } from 'msw';
import { products, orders } from './mock';
import Utils from '../utils/utils';

const handlers = [
  rest.get('/products', async (req, res, ctx) => {
    return res(ctx.delay(200), ctx.status(200), ctx.json(products));
  }),
  rest.get('/products/:id', async (req, res, ctx) => {
    const { id } = req.params;
    const product = products.find((pr) => +pr.id === +id);
    return res(ctx.delay(200), ctx.status(200), ctx.json(product));
  }),
  rest.post('/checkout/pay', async (req, res, ctx) => {
    const listOrders = Utils.getLocalStorageItem('orders', []);
    const orderId = listOrders.length + 1;
    listOrders.push({ orderId, ...JSON.parse(req.body) });
    Utils.setLocalStorageItem('orders', listOrders);
    return res(ctx.delay(200), ctx.status(200));
  }),
  rest.get('/orders', async (req, res, ctx) => {
    const listOrders = Utils.getLocalStorageItem('orders', []);
    const ordersResult = process.env.NODE_ENV === 'test' || window.Cypress ? orders : listOrders;
    return res(ctx.delay(200), ctx.status(200), ctx.json(ordersResult));
  }),
];

export default handlers;
