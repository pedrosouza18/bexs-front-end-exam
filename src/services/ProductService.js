export default {
  abortableFetch(request, opts) {
    const controller = new AbortController();
    const { signal } = controller;

    return {
      abort: () => controller.abort(),
      ready: fetch(request, { ...opts, signal }),
    };
  },
  getProducts() {
    return { ...this.abortableFetch('/products') };
  },
  getProductById(id) {
    return { ...this.abortableFetch(`/products/${id}`) };
  },
  pay(payload) {
    return {
      ...this.abortableFetch('/checkout/pay', {
        method: 'POST',
        body: JSON.stringify(payload),
      }),
    };
  },
  getOrders() {
    return { ...this.abortableFetch('/orders') };
  },
};
